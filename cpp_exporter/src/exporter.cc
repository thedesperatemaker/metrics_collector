#include <memory>
#include <array>
#include <chrono>
#include <cstdlib>
#include <string>
#include <thread>
 
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"

using std::placeholders::_1;

#ifdef _WIN32
#include <Winsock2.h>
#else
#include <unistd.h>
#endif

#include "prometheus/gauge.h"
#include "prometheus/family.h"
#include "prometheus/gateway.h"

#include <prometheus/counter.h>
#include <prometheus/exposer.h>
#include <prometheus/registry.h>
 
/* This example creates a subclass of Node and uses std::bind() to register a
* member function as a callback from the timer. */
using namespace prometheus;
  
static std::string GetHostName() {
  char hostname[1024];

  if (::gethostname(hostname, sizeof(hostname))) {
    return {};
  }
  return hostname;
}

prometheus::Exposer exposer{"0.0.0.0:5000"};
auto registry = std::make_shared<Registry>();
// add a new counter family to the registry (families combine values with the
// same name, but distinct label dimensions)
//
// @note please follow the metric-naming best-practices:
// https://prometheus.io/docs/practices/naming/
auto& packet_counter = BuildCounter()
                            .Name("counter_1")
                            .Help("Number of observed packets")
                            .Register(*registry);
 
// add a counter whose dimensional data is not known at compile time
// nevertheless dimensional values should only occur in low cardinality:
// https://prometheus.io/docs/practices/naming/#labels
// auto registry = std::make_shared<Registry>();
  auto& gauge_family = BuildGauge()
                         .Name("some_name_1")
                         .Help("Additional description.")
                         .Labels({{"key", "value"}})
                         .Register(*registry);

  

class MinimalSubscriber : public rclcpp::Node
{
  public:
    MinimalSubscriber()
    : Node("minimal_subscriber")
    {
      RCLCPP_INFO(this->get_logger(), "1 ");

      subscription_ = this->create_subscription<std_msgs::msg::String>(
                                               "topic", 10, std::bind(&MinimalSubscriber::topic_callback, this, _1));    
      RCLCPP_INFO(this->get_logger(), "2 ");
    }

  private:
    void topic_callback(const std_msgs::msg::String::SharedPtr msg)
    {
    //   std::string s = "Example_45-3";
    //   int p1 = &msg.find("!")+1;
    //   int p2 = &msg.lengh();
      std::string number = msg->data.substr(13, msg->data.length());

    //   int k = &msg.find('!');
      gauge_family.Add({{"key2", "gauge_family"}}).Set(1./(1+stoi( msg->data.substr(14, msg->data.length()))));
      packet_counter.Add({{"key", "packet_counter"}}).Increment();
      RCLCPP_INFO(this->get_logger(), "Metrics pushed '%f'", 1./(1+stoi( msg->data.substr(14, msg->data.length()))) );
    }

    // create a push gateway as class member
    // const std::map<std::string, std::string> labels = Gateway::GetInstanceLabel(GetHostName());
    // Gateway gateway{"127.0.0.1", "9091", "sample_client", labels};

    // std::shared_ptr<prometheus::Registry> registry;
    // std::shared_ptr<prometheus::Counter> second_counter;

    rclcpp::Subscription<std_msgs::msg::String>::SharedPtr subscription_;
};

int main(int argc, char * argv[])
{
  exposer.RegisterCollectable(registry);

  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<MinimalSubscriber>());
  rclcpp::shutdown();
  
  return 0;
}
