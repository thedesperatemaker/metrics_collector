# app.py
import time

from flask import Flask, request, Response
from prometheus_client.core import GaugeMetricFamily, REGISTRY
from prometheus_client import generate_latest, CONTENT_TYPE_LATEST, make_wsgi_app

app = Flask(__name__)

class CustomServiceExporter:
    temp1 = {}
    request_count = {}
    gauge_metric = {}

    def collect(self):
        cur = time.time()
        temperature_ = GaugeMetricFamily(
            "temp1",
            "temperature",
            # label=["measure"]
        )
        gauge_family = GaugeMetricFamily(
            "gauge_metric",
            "metric",
            # label=["measure"]
        )

        for i in CustomServiceExporter.temp1.keys():
            temperature_.add_metric(labels="temperature",value=CustomServiceExporter.temp1[i],timestamp=time.time())
        yield temperature_

        for i in CustomServiceExporter.gauge_metric.keys():
            gauge_family.add_metric(labels="gauge_metric",value=CustomServiceExporter.gauge_metric[i],timestamp=time.time())
        yield gauge_family


REGISTRY.register(CustomServiceExporter())

@app.route("/metrics-receiver", methods=["POST"])
def track_metric():
    data = request.json
    cur_time = time.strftime('%Y.%m.%d_%H:%M:%S', time.localtime(time.time()))
    CustomServiceExporter.temp1[cur_time] = str(data["measure"])
    CustomServiceExporter.gauge_metric[cur_time] = str(data["measure"])
    # for i in CustomServiceExporter.temp1.keys():
    #     print(CustomServiceExporter.temp1[i])
    return "ok"

@app.route("/metrics")
def metrics():
    return Response(generate_latest(),mimetype=CONTENT_TYPE_LATEST)

if __name__ == "__main__":
    app.run("0.0.0.0", 5000, threaded=True)
