import time
import requests
import json

import rclpy
from rclpy.node import Node
# from sweeper_msgs.msg import CleaningEquipmentFeedback  
from std_msgs.msg import String

HOST = "http://app:5000/"

class MinimalSubscriber(Node):
    def __init__(self):
        super().__init__('equipment_subscriber')
        self.subscription = self.create_subscription(
            String,
            'topic',
            self._callback,
            10)
        self.subscription

    def _callback(self, msg):
        self.get_logger().warn('Publishing: "%s"' % msg.data)
        # temp1 = msg.low.temperature_right_brush
        # payload = {"time": time.time(), "measure": temp1}
        metric_ = 123 
        payload = {"time": time.time(), "measure": metric_}
        try:
            target = "/metrics-receiver"
            # requests.get(HOST + target, timeout=1)
            requests.post(HOST + target, json = payload)
        except requests.RequestException:
            print("cannot connect", HOST)
            time.sleep(1)

def main(args=None):
    rclpy.init(args=args)

    minimal_subscriber = MinimalSubscriber()
    rclpy.spin(minimal_subscriber)

if __name__ == "__main__":
    main()
